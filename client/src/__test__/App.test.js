import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App';
import Home from '../components/home/Index';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App ><Home /></App>, div);
});
