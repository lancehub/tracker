import request from 'superagent';
import config from '../config';
import auth from './auth';
import users from './users';
import expenses from './expenses';
import stats from './stats';

export default {
  online() {
    return request.get(`${config.api_base}/`);
  },
  auth,
  users,
  expenses,
  stats,
};
