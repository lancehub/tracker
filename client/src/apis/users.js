import request from 'superagent';
import store from 'store';
import config from '../config';

export default {
  index(page, queries) {
    return request
      .get(`${config.api_base}/users`)
      .query({ page })
      .query(queries)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
  add(data) {
    return request
      .post(`${config.api_base}/users`)
      .set('Authorization', `Bearer ${store.get('token')}`)
      .query(data);
  },
  edit(id, data) {
    return request
      .put(`${config.api_base}/users/${id}`)
      .set('Authorization', `Bearer ${store.get('token')}`)
      .query(data);
  },
  view(id) {
    return request
      .get(`${config.api_base}/users/${id}`)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
  delete(id) {
    return request
      .delete(`${config.api_base}/users/${id}`)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
};
