import request from 'superagent';
import store from 'store';
import config from '../config';

export default {
  index(page, queries) {
    return request
      .get(`${config.api_base}/expenses`)
      .query({ page })
      .query(queries)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
  add(data) {
    return request
      .post(`${config.api_base}/expenses`)
      .set('Authorization', `Bearer ${store.get('token')}`)
      .query(data);
  },
  edit(id, data) {
    return request
      .put(`${config.api_base}/expenses/${id}`)
      .set('Authorization', `Bearer ${store.get('token')}`)
      .query(data);
  },
  view(id) {
    return request
      .get(`${config.api_base}/expenses/${id}`)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
  delete(id) {
    return request
      .delete(`${config.api_base}/expenses/${id}`)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
};
