import request from 'superagent';
import store from 'store';
import config from '../config';

export default {
  index(queries) {
    return request
      .get(`${config.api_base}/stats`)
      .query(queries)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
  view(id, queries) {
    return request
      .get(`${config.api_base}/stats/${id}`)
      .query(queries)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
};
