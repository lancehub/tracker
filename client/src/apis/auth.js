import request from 'superagent';
import store from 'store';
import config from '../config';

export default {
  login(data) {
    return request
      .post(`${config.api_base}/auth/login`)
      .query(data);
  },
  register(data) {
    return request
      .post(`${config.api_base}/auth/register`)
      .query(data);
  },
  me() {
    return request
      .get(`${config.api_base}/auth/me`)
      .set('Authorization', `Bearer ${store.get('token')}`);
  },
};
