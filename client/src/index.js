import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import App from './components/App';
import Home from './components/home/Index';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Profile from './components/account/Profile';
import AccountEdit from './components/account/Edit';
import ExpensesIndex from './components/expenses/Index';
import ExpensesAdd from './components/expenses/Add';
import ExpensesEdit from './components/expenses/Edit';
import ExpensesView from './components/expenses/View';
import UsersIndex from './components/users/Index';
import UsersAdd from './components/users/Add';
import UsersEdit from './components/users/Edit';
import UsersView from './components/users/View';
import auth_utils from './utils/auth';

import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/react-datetime/css/react-datetime.css';

ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="login" component={Login} />
      <Route path="register" component={Register} />
      <Route path="account" onEnter={auth_utils.require_login}>
        <IndexRoute component={Profile} />
        <Route path="edit" component={AccountEdit} />
      </Route>
      <Route path="expenses" onEnter={auth_utils.require_login}>
        <IndexRoute component={ExpensesIndex} />
        <Route path="add" component={ExpensesAdd} />
        <Route path="edit/:id" component={ExpensesEdit} />
        <Route path="view/:id" component={ExpensesView} />
      </Route>
      <Route path="users" onEnter={auth_utils.require_non_regular}>
        <IndexRoute component={UsersIndex} />
        <Route path="add" component={UsersAdd} />
        <Route path="edit/:id" component={UsersEdit} />
        <Route path="view/:id" component={UsersView} />
      </Route>
    </Route>
  </Router>,
  document.getElementById('root'),
);
