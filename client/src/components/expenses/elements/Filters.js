import React, { Component } from 'react';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Collapse from 'react-bootstrap/lib/Collapse';
import Well from 'react-bootstrap/lib/Well';
import DateTime from 'react-datetime';

class Filters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        description: '',
        amount_from: '',
        amount_to: '',
        pay_at_from: '',
        pay_at_to: '',
      },
    };
    this.handleChangeFn = this.handleChangeFn.bind(this);
    this.handleChangeDateTimeFn = this.handleChangeDateTimeFn.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillMount() {
    this.setState({
      fields: Object.assign({}, this.state.fields, this.props.fields),
    });
  }
  handleChangeFn(field) {
    const handleFn = function handleFn(event) {
      const fields = this.state.fields;
      fields[field] = event.target.value;
      this.setState({ fields });
    };
    return handleFn.bind(this);
  }
  handleChangeDateTimeFn(field) {
    const handleFn = function handleFn(moment) {
      const fields = this.state.fields;
      if (moment) {
        fields[field] = moment.format('YYYY-MM-DD');
      } else {
        fields[field] = '';
      }
      this.setState({ fields });
    };
    return handleFn.bind(this);
  }
  handleSubmit() {
    this.props.onSearch(this.state.fields);
  }
  render() {
    return (
      <div>
        <ButtonToolbar>
          <Button onClick={() => this.setState({ open: !this.state.open })}>
            <Glyphicon glyph="filter" /> Filters
          </Button>
        </ButtonToolbar>
        <br />
        <Collapse in={this.state.open}>
          <div>
            <Well>
              <Form>
                <FormGroup controlId="formInlineName">
                  <Row>
                    <Col xs={12} sm={4} md={4}>
                      <ControlLabel>Description</ControlLabel>
                    </Col>
                    <Col xs={12} sm={4} md={4}>
                      <FormControl placeholder="Description" value={this.state.fields.description} onChange={this.handleChangeFn('description')} />
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup controlId="formInlineEmail">
                  <Row>
                    <Col xs={12} sm={4} md={4}>
                      <ControlLabel>Amount Range</ControlLabel>
                    </Col>
                    <Col xs={6} sm={4} md={4}>
                      <FormControl placeholder="From" value={this.state.fields.amount_from} onChange={this.handleChangeFn('amount_from')} />
                    </Col>
                    <Col xs={6} sm={4} md={4}>
                      <FormControl placeholder="To" value={this.state.fields.amount_to} onChange={this.handleChangeFn('amount_to')} />
                    </Col>
                  </Row>
                </FormGroup>
                <FormGroup controlId="formInlineEmail">
                  <Row>
                    <Col xs={12} sm={4} md={4}>
                      <ControlLabel>DateTime Range</ControlLabel>
                    </Col>
                    <Col xs={6} sm={4} md={4}>
                      <DateTime value={this.state.fields.pay_at_from} onChange={this.handleChangeDateTimeFn('pay_at_from')} dateFormat="YYYY-MM-DD" timeFormat="HH:mm:ss" closeOnSelect inputProps={{ placeholder: 'Date From' }} />
                    </Col>
                    <Col xs={6} sm={4} md={4}>
                      <DateTime value={this.state.fields.pay_at_to} onChange={this.handleChangeDateTimeFn('pay_at_to')} dateFormat="YYYY-MM-DD" timeFormat="HH:mm:ss" closeOnSelect inputProps={{ placeholder: 'Date To' }} />
                    </Col>
                  </Row>
                </FormGroup>
                <Button bsStyle="primary" onClick={this.handleSubmit}>Search</Button>
              </Form>
            </Well>
          </div>
        </Collapse>
      </div>
    );
  }
}

export default Filters;
