import React, { Component } from 'react';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import ButtonGroup from 'react-bootstrap/lib/ButtonGroup';
import store from 'store';

import List from '../common/List';
import Filters from './elements/Filters';
import TableHeader from '../common/TableHeader';

import rest from '../../apis/expenses';


class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mode: 'own',
      filters: {},
      sort: 'id,desc',
    };
    this.handleFilters = this.handleFilters.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.searchFn = this.searchFn.bind(this);
    this.renderItem = this.renderItem.bind(this);
  }
  handleFilters(filters) {
    this.setState({ filters });
  }
  handleSort(sort) {
    this.setState({ sort });
  }
  searchFn(page) {
    return rest.index(page, this.state);
  }
  renderItem(expense) {
    return (
      <tr key={expense.id} onClick={() => (window.location = `#/expenses/view/${expense.id}`)}>
        <td>{expense.id}</td>
        {this.state.mode === 'manage' && <td>{expense.user.name}</td>}
        <td>{expense.amount}</td>
        <td>{expense.pay_at}</td>
        <td>{expense.description}</td>
      </tr>
    );
  }
  render() {
    const headerFields = { id: '#', user_id: 'User', amount: 'Amount', pay_at: 'DateTime', description: 'Description' };
    if (this.state.mode !== 'manage') {
      delete headerFields.user_id;
    }
    const tableHeader = (<TableHeader
      onSort={this.handleSort}
      sort={this.state.sort}
      fields={headerFields}
    />);
    return (
      <Row>
        <Col xs={12} sm={12} md={12}>
          <Filters filters={this.state.filters} onSearch={this.handleFilters} />
          <List
            tableHeader={tableHeader}
            renderItem={this.renderItem}
            fetchData={this.searchFn}
          />
          <Row>
            <Col xs={12} sm={6} md={6}>
              <ButtonToolbar>
                <Button onClick={() => (window.location = '#/expenses/add')}>Add</Button>
              </ButtonToolbar>
            </Col>
            <Col xs={12} sm={6} md={6} style={{ textAlign: 'right' }}>
              {store.get('role') === 'admin' && <ButtonGroup>
                <Button onClick={() => this.setState({ mode: 'own' })} bsStyle={this.state.mode === 'own' ? 'success' : 'default'}>Own Mode</Button>
                <Button onClick={() => this.setState({ mode: 'manage' })} bsStyle={this.state.mode === 'manage' ? 'success' : 'default'}>Manage Mode</Button>
                </ButtonGroup>}
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default Index;
