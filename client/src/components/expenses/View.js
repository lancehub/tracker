import React, { Component } from 'react';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Table from 'react-bootstrap/lib/Table';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';

import rest from '../../apis/expenses';

class View extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expense: null,
    };
    this.handleDelete = this.handleDelete.bind(this);
  }
  componentDidMount() {
    this.fetchData();
  }
  fetchData() {
    rest.view(this.props.params.id).end((err, resp) => {
      if (resp.status === 200) {
        this.setState({
          expense: resp.body.expense,
        });
      }
    });
  }
  handleDelete() {
    if (confirm('Are you sure to delete this record?')) {
      rest.delete(this.props.params.id).end((err, resp) => {
        if (resp.status === 204) {
          alert('Delete Success!');
          window.location = '#/expenses';
        }
      });
    }
  }
  render() {
    return (
      <Row>
        <Col xs={12} sm={8} smOffset={2} md={6} mdOffset={3}>
          <Table striped bordered condensed hover>
            <thead>
              <tr>
                <th>Key</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>ID</td>
                <td>{this.state.expense && this.state.expense.id}</td>
              </tr>
              <tr>
                <td>User ID</td>
                <td>{this.state.expense && this.state.expense.user_id}</td>
              </tr>
              <tr>
                <td>Description</td>
                <td>{this.state.expense && this.state.expense.description}</td>
              </tr>
              <tr>
                <td>Amount</td>
                <td>{this.state.expense && this.state.expense.amount}</td>
              </tr>
              <tr>
                <td>DateTime</td>
                <td>{this.state.expense && this.state.expense.pay_at}</td>
              </tr>
              <tr>
                <td>Comment</td>
                <td>{this.state.expense && this.state.expense.comment}</td>
              </tr>
            </tbody>
          </Table>
          <ButtonToolbar>
            <Button onClick={() => (window.location = `#/expenses/edit/${this.props.params.id}`)}>Edit</Button>
            <Button onClick={this.handleDelete} bsStyle="danger">Delete</Button>
          </ButtonToolbar>
        </Col>
      </Row>
    );
  }
}

export default View;
