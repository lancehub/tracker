import React, { Component } from 'react';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Button from 'react-bootstrap/lib/Button';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Panel from 'react-bootstrap/lib/Panel';
import Alert from 'react-bootstrap/lib/Alert';

import DateTime from 'react-datetime';

import validate from 'validate.js';
import store from 'store';
import rest from '../../apis/expenses';
import validation_rules from '../../utils/validation_rules';

class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        description: '',
        amount: '',
        pay_at: '',
        comment: '',
        user_id: '',
      },
      alert: '',
      field_errors: {},
    };

    this.handleChangeFn = this.handleChangeFn.bind(this);
    this.handleChangeDateTime = this.handleChangeDateTime.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    this.fetchData();
  }
  fetchData() {
    rest.view(this.props.params.id).end((err, resp) => {
      if (resp.status === 200) {
        this.setState({
          fields: resp.body.expense,
        });
      }
    });
  }
  handleChangeFn(field) {
    const handleFn = function handleFn(event) {
      const fields = this.state.fields;
      fields[field] = event.target.value;
      this.setState({ fields });
    };
    return handleFn.bind(this);
  }
  handleChangeDateTime(moment) {
    const fields = this.state.fields;
    fields.pay_at = moment.format('YYYY-MM-DD HH:mm:ss');
    this.setState({ fields });
  }
  validate() {
    this.setState({
      alert: null,
      field_errors: {},
    });
    const errors = validate(this.state.fields, validation_rules.expense);
    if (errors) {
      setTimeout(() =>
        this.setState({
          alert: 'Error, please check following errors:',
          field_errors: errors,
        })
        , 100);
      return false;
    }
    return true;
  }
  handleSubmit() {
    if (this.validate()) {
      rest.edit(this.props.params.id, this.state.fields)
        .end((err, resp) => {
          if (resp.status === 400) {
            this.setState({
              alert: 'Error, please check following errors:',
              field_errors: resp.body.messages,
            });
          } else {
            alert('Save Success!');
            window.location = '#/expenses';
          }
        });
    }
  }
  render() {
    return (<Row>
      <Col xs={12} sm={8} smOffset={2} md={6} mdOffset={3}>
        {this.state.alert && <Alert bsStyle="warning">
          {this.state.alert}
        </Alert>}
        <Panel header="Edit Expense">
          <Form>
            {store.get('role') === 'admin' && <FormGroup validationState={this.state.field_errors.user_id && 'error'}>
              <ControlLabel>User ID</ControlLabel>
              <FormControl placeholder="User ID" value={this.state.fields.user_id} onChange={this.handleChangeFn('user_id')} />
              {this.state.field_errors.user_id && <HelpBlock>{this.state.field_errors.user_id[0]}</HelpBlock>}
            </FormGroup>}

            <FormGroup validationState={this.state.field_errors.description && 'error'}>
              <ControlLabel>Description</ControlLabel>
              <FormControl placeholder="Name" value={this.state.fields.description} onChange={this.handleChangeFn('description')} />
              {this.state.field_errors.description && <HelpBlock>{this.state.field_errors.description[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup validationState={this.state.field_errors.amount && 'error'}>
              <ControlLabel>Amount</ControlLabel>
              <FormControl placeholder="Amount" value={this.state.fields.amount} onChange={this.handleChangeFn('amount')} />
              {this.state.field_errors.amount && <HelpBlock>{this.state.field_errors.amount[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup validationState={this.state.field_errors.pay_at && 'error'}>
              <ControlLabel>Pay At</ControlLabel>
              <DateTime value={this.state.fields.pay_at} onChange={this.handleChangeDateTime} dateFormat="YYYY-MM-DD" timeFormat="HH:mm:ss" inputProps={{ placeholder: 'Date & Time' }} />
              {this.state.field_errors.pay_at && <HelpBlock>{this.state.field_errors.pay_at[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup validationState={this.state.field_errors.comment && 'error'}>
              <ControlLabel>Comment</ControlLabel>
              <FormControl componentClass="textarea" placeholder="Comment" value={this.state.fields.comment} onChange={this.handleChangeFn('comment')} />
              {this.state.field_errors.comment && <HelpBlock>{this.state.field_errors.comment[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup>
              <Button bsStyle="primary" onClick={this.handleSubmit}>
                Save
                </Button>
            </FormGroup>
          </Form>
        </Panel>
      </Col>
    </Row>);
  }
}

export default Edit;
