import React, { Component } from 'react';
import Panel from 'react-bootstrap/lib/Panel';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';

import store from 'store';
import stats from '../../../apis/stats';


class Stats extends Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      average: 0,
    };
  }
  componentDidMount() {
    this.fetchData(this.props);
  }
  componentWillReceiveProps(props) {
    this.fetchData(props);
  }
  fetchData(props) {
    if (props.mode === 'my') {
      stats.view(store.get('id'), { from: props.from, to: props.to }).end((err, resp) => {
        this.setState(resp.body);
      });
    } else {
      stats.index({ from: props.from, to: props.to }).end((err, resp) => {
        this.setState(resp.body);
      });
    }
  }
  render() {
    return (
      <Panel header={this.props.title} bsStyle={this.props.titleClass}>
        <Row>
          <Col xs={6} sm={6} md={6}>Total: {this.state.total}</Col>
          <Col xs={6} sm={6} md={6}>Average: {this.state.average}</Col>
        </Row>
      </Panel>
    );
  }
}

export default Stats;
