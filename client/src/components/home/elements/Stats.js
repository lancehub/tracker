import React, { Component } from 'react';
import Pager from 'react-bootstrap/lib/Pager';
import store from 'store';
import moment from 'moment';
import Stat from './Stat';

class Stats extends Component {
  constructor(props) {
    super(props);
    this.state = {
      from: moment().startOf('week').format('YYYY-MM-DD'),
      to: moment().startOf('week').add(7, 'd').format('YYYY-MM-DD'),
    };
    this.prevWeek = this.prevWeek.bind(this);
    this.nextWeek = this.nextWeek.bind(this);
  }
  prevWeek() {
    this.setState({
      from: moment(this.state.from).subtract(7, 'd').format('YYYY-MM-DD'),
      to: moment(this.state.to).subtract(7, 'd').format('YYYY-MM-DD'),
    });
  }
  nextWeek() {
    this.setState({
      from: moment(this.state.from).add(7, 'd').format('YYYY-MM-DD'),
      to: moment(this.state.to).add(7, 'd').format('YYYY-MM-DD'),
    });
  }
  render() {
    return (
      <div>
        <p>Stats from {this.state.from} to {this.state.to}</p>
        {store.get('role') === 'admin' && <Stat from={this.state.from} to={this.state.to} mode="system" title="System Stats" titleClass="success" />}
        <Stat from={this.state.from} to={this.state.to} mode="my" title="My Stats" titleClass="primary" />

        <Pager>
          <Pager.Item onClick={this.prevWeek}>Previous</Pager.Item>
          {' '}
          <Pager.Item onClick={this.nextWeek}>Next</Pager.Item>
        </Pager>
      </div>
    );
  }
}

export default Stats;
