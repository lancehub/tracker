import React, { Component } from 'react';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Alert from 'react-bootstrap/lib/Alert';
import Stats from './elements/Stats';

import flash from '../../utils/flash';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: '',
    };
  }
  componentDidMount() {
    this.setMessage();
  }
  componentWillReceiveProps() {
    this.setMessage();
  }
  setMessage() {
    const message = flash.get('message');
    if (message) {
      this.setState({
        message,
      });
    }
    if (!this.props.loggedin && !message) {
      this.setState({
        message: 'Please login to use this app!',
      });
    }
  }
  render() {
    return (
      <Row>
        <Col xs={12} sm={12} md={12}>
          {this.state.message && <Alert>{this.state.message}</Alert>}
          {this.props.loggedin && <Stats />}
        </Col>
      </Row>
    );
  }
}

export default Home;
