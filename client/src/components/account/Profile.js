import React, { Component } from 'react';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Table from 'react-bootstrap/lib/Table';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';

import auth from '../../apis/auth';

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
    };
  }
  componentDidMount() {
    this.fetchData();
  }
  fetchData() {
    auth.me().end((err, resp) => {
      if (resp.status === 200) {
        this.setState({
          user: resp.body.user,
        });
      }
    });
  }
  render() {
    return (
      <Row>
        <Col xs={12} sm={8} smOffset={2} md={6} mdOffset={3}>
          <Table striped bordered condensed hover>
            <thead>
              <tr>
                <th>Key</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>ID</td>
                <td>{this.state.user && this.state.user.id}</td>
              </tr>
              <tr>
                <td>Name</td>
                <td>{this.state.user && this.state.user.name}</td>
              </tr>
              <tr>
                <td>Email</td>
                <td>{this.state.user && this.state.user.email}</td>
              </tr>
              <tr>
                <td>Role</td>
                <td>{this.state.user && this.state.user.role}</td>
              </tr>
              <tr>
                <td>Created At</td>
                <td>{this.state.user && this.state.user.created_at}</td>
              </tr>
            </tbody>
          </Table>
          <ButtonToolbar>
            <Button onClick={() => (window.location = '#/account/edit')}>Edit</Button>
          </ButtonToolbar>
        </Col>
      </Row>
    );
  }
}

export default Profile;
