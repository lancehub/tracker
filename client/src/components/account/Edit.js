import React, { Component } from 'react';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Button from 'react-bootstrap/lib/Button';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Panel from 'react-bootstrap/lib/Panel';
import Alert from 'react-bootstrap/lib/Alert';
import validate from 'validate.js';

import PasswordShowHide from '../common/PasswordShowHide';
import auth from '../../apis/auth';
import rest from '../../apis/users';
import validation_rules from '../../utils/validation_rules';

class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        name: '',
        email: '',
        password: '',
        role: 'regular',
      },
      alert: '',
      field_errors: {},
    };

    this.handleChangeFn = this.handleChangeFn.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    this.fetchData();
  }
  fetchData() {
    auth.me().end((err, resp) => {
      if (resp.status === 200) {
        this.setState({
          fields: resp.body.user,
        });
      }
    });
  }
  handleChangeFn(field) {
    const handleFn = function handleFn(event) {
      const fields = this.state.fields;
      fields[field] = event.target.value;
      this.setState({ fields });
    };
    return handleFn.bind(this);
  }
  validate() {
    this.setState({
      alert: null,
      field_errors: {},
    });
    const errors = validate(this.state.fields, validation_rules.user);
    if (errors) {
      setTimeout(() =>
        this.setState({
          alert: 'Error, please check following errors:',
          field_errors: errors,
        })
        , 100);
      return false;
    }
    return true;
  }
  handleSubmit() {
    if (this.validate()) {
      rest.edit(this.state.fields.id, this.state.fields)
        .end((err, resp) => {
          if (resp.status === 400) {
            this.setState({
              alert: 'Error, please check following errors:',
              field_errors: resp.body.messages,
            });
          } else {
            alert('Save Success!');
            window.location = '#/account';
          }
        });
    }
  }
  render() {
    return (<Row>
      <Col xs={12} sm={8} smOffset={2} md={6} mdOffset={3}>
        {this.state.alert && <Alert bsStyle="warning">
          {this.state.alert}
        </Alert>}
        <Panel header="Edit User">
          <Form>
            <FormGroup validationState={this.state.field_errors.name && 'error'}>
              <ControlLabel>Name</ControlLabel>
              <FormControl type="text" placeholder="Name" value={this.state.fields.name} onChange={this.handleChangeFn('name')} />
              {this.state.field_errors.name && <HelpBlock>{this.state.field_errors.name[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup validationState={this.state.field_errors.email && 'error'}>
              <ControlLabel>Email</ControlLabel>
              <FormControl type="email" placeholder="Email" value={this.state.fields.email} onChange={this.handleChangeFn('email')} />
              {this.state.field_errors.email && <HelpBlock>{this.state.field_errors.email[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup validationState={this.state.field_errors.password && 'error'}>
              <ControlLabel>Password</ControlLabel>
              <PasswordShowHide placeholder="Password" value={this.state.fields.password} onChange={this.handleChangeFn('password')} />
              {this.state.field_errors.password && <HelpBlock>{this.state.field_errors.password[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup>
              <Button bsStyle="primary" onClick={this.handleSubmit}>
                Save
                </Button>
            </FormGroup>
          </Form>
        </Panel>
      </Col>
    </Row>);
  }
}

export default Edit;
