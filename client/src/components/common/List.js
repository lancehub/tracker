import React, { Component } from 'react';

import Table from 'react-bootstrap/lib/Table';
import Pagination from 'react-bootstrap/lib/Pagination';
import Loading from '../common/Loading';

const initialState = {
  loading: true,
  records: [],
  meta: {
    last_page: 1,
  },
  page: 1,
};

class List extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this.handlePageSelect = this.handlePageSelect.bind(this);
  }
  componentDidMount() {
    this.fetchData(1);
  }
  componentWillReceiveProps() {
    this.setState(initialState);
    this.fetchData(1);
  }
  fetchData(page) {
    this.setState({
      loading: true,
    });
    this.props.fetchData(page).end((err, resp) => {
      if (resp.status === 200) {
        this.setState({
          loading: false,
          records: resp.body.data,
          meta: resp.body.meta,
        });
      }
    });
  }
  handlePageSelect(selectPage) {
    this.setState({
      page: selectPage,
    });
    this.fetchData(selectPage);
  }
  render() {
    return (
      <div>
        {this.state.loading && <Loading />}
        {!this.state.loading && this.state.records.length === 0 && 'No Records Found'}
        {!this.state.loading && this.state.records.length > 0 && <Table striped bordered condensed hover responsive>
          {this.props.tableHeader}
          <tbody>
            {this.state.records.map(record => this.props.renderItem(record))}
          </tbody>
        </Table>}
        {!this.state.loading && this.state.records.length > 0 && this.state.meta.last_page > 1 && <Pagination
          bsSize="medium"
          items={this.state.meta.last_page}
          activePage={this.state.page}
          ellipsis={this.state.meta.last_page > 10}
          prev={this.state.page > 1}
          next={this.state.page < this.state.meta.last_page}
          onSelect={this.handlePageSelect}
        />}
      </div>
    );
  }
}

export default List;
