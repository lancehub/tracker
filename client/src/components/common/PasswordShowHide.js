import React, { Component } from 'react';
import FormControl from 'react-bootstrap/lib/FormControl';
import InputGroup from 'react-bootstrap/lib/InputGroup';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';

class PasswordShowHide extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
    this.toggle = this.toggle.bind(this);
  }
  toggle() {
    this.setState({
      show: !this.state.show,
    });
  }
  render() {
    return (
      <InputGroup>
        <FormControl {...this.props} type={this.state.show ? 'text' : 'password'} />
        <InputGroup.Addon onClick={this.toggle}><Glyphicon glyph={this.state.show ? 'eye-close' : 'eye-open'} /></InputGroup.Addon>
      </InputGroup>
    );
  }
}

export default PasswordShowHide;
