import React, { Component } from 'react';
import Navbar from 'react-bootstrap/lib/Navbar';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import store from 'store';
import auth_utils from '../../utils/auth';

class AppBar extends Component {
  logout() {
    auth_utils.logout();
    this.props.onRefresh();
  }
  render() {
    return (
      <Navbar collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="#/">
              Expenses Tracker
            </a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          {this.props.loggedin && <Nav>
            <NavItem onSelect={() => (window.location = '#/expenses')}>Expenses</NavItem>
            {store.get('role') !== 'regular' && <NavItem onSelect={() => (window.location = '#/users')}>Users</NavItem>}
          </Nav>}
          {this.props.loggedin ? <Nav pullRight>
            <NavItem onSelect={() => (window.location = '#/account')}>{store.get('name')}</NavItem>
            <NavItem onSelect={() => this.logout()}>Logout</NavItem>
          </Nav> : <Nav pullRight>
            <NavItem onSelect={() => (window.location = '#/login')}>Login</NavItem>
            <NavItem onSelect={() => (window.location = '#/register')}>Register</NavItem>
          </Nav>}
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default AppBar;

