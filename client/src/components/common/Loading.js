import React, { Component } from 'react';
import Label from 'react-bootstrap/lib/Label';

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dots: '',
    };
  }
  componentDidMount() {
    this.timer = setInterval(() => {
      if (this.state.dots.length > 10) {
        this.setState({
          dots: '',
        });
      } else {
        this.setState({
          dots: `${this.state.dots}.`,
        });
      }
    }, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  render() {
    return (<div style={{ margin: '30px 0 50px' }}>
      <Label bsStyle="primary">Loading {this.state.dots}</Label>
    </div>);
  }
}

export default Loading;
