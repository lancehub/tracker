import React, { Component } from 'react';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';

class TableHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order_by: 'id',
      order_dir: 'desc',
    };
    this.handleSortFn = this.handleSortFn.bind(this);
  }
  componentWillMount() {
    const [order_by, order_dir] = this.props.sort.split(',');
    this.setState(Object.assign({}, this.state, { order_by, order_dir }));
  }
  handleSortFn(key) {
    const handleFn = function handleFn() {
      let sort = {};
      if (this.state.order_by === key) {
        sort = {
          order_by: key,
          order_dir: this.state.order_dir === 'asc' ? 'desc' : 'asc',
        };
      } else {
        sort = {
          order_by: key,
          order_dir: 'desc',
        };
      }
      this.setState(sort);
      this.props.onSort(`${sort.order_by},${sort.order_dir}`);
    };
    return handleFn.bind(this);
  }
  render() {
    return (
      <thead>
        <tr>
          {Object.keys(this.props.fields).map(key => <th key={key} onClick={this.handleSortFn(key)}>
            {this.props.fields[key]}
            {this.state.order_by === key && <Glyphicon glyph={this.state.order_dir === 'asc' ? 'sort-by-attributes' : 'sort-by-attributes-alt'} />}
          </th>)}
        </tr>
      </thead>
    );
  }
}

export default TableHeader;
