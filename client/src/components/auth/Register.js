import React, { Component } from 'react';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Button from 'react-bootstrap/lib/Button';
import HelpBlock from 'react-bootstrap/lib/HelpBlock';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Panel from 'react-bootstrap/lib/Panel';
import Alert from 'react-bootstrap/lib/Alert';

import validate from 'validate.js';
import auth from '../../apis/auth';
import auth_utils from '../../utils/auth';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        name: '',
        email: '',
        password: '',
        password_verify: '',
      },
      alert: '',
      field_errors: {},
    };

    this.handleChangeFn = this.handleChangeFn.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChangeFn(field) {
    const handleFn = function handleFn(event) {
      const fields = this.state.fields;
      fields[field] = event.target.value;
      this.setState({ fields });
    };
    return handleFn.bind(this);
  }
  validate() {
    this.setState({
      alert: null,
      field_errors: {},
    });
    const errors = validate(this.state.fields, {
      name: {
        presence: true,
      },
      email: {
        presence: true,
        email: true,
      },
      password: {
        presence: true,
      },
      password_verify: {
        presence: true,
        equality: {
          attribute: 'password',
        },
      },
    });
    if (errors) {
      setTimeout(() =>
        this.setState({
          alert: 'Error, please check following errors:',
          field_errors: errors,
        })
        , 100);
      return false;
    }
    return true;
  }
  handleSubmit() {
    if (this.validate()) {
      auth.register(this.state.fields)
        .end((err, resp) => {
          if (resp.status === 400) {
            this.setState({
              alert: 'Error, please check following errors:',
              field_errors: resp.body.messages,
            });
          } else {
            auth_utils.login(resp.body, 'Register Success!');
          }
        });
    }
  }
  render() {
    return (<Row>
      <Col xs={12} sm={8} smOffset={2} md={6} mdOffset={3}>
        {this.state.alert && <Alert bsStyle="warning">
          {this.state.alert}
        </Alert>}
        <Panel header="Register">
          <Form>
            <FormGroup validationState={this.state.field_errors.name && 'error'}>
              <ControlLabel>Name</ControlLabel>
              <FormControl type="text" placeholder="Name" value={this.state.fields.name} onChange={this.handleChangeFn('name')} />
              {this.state.field_errors.name && <HelpBlock>{this.state.field_errors.name[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup validationState={this.state.field_errors.email && 'error'}>
              <ControlLabel>Email</ControlLabel>
              <FormControl type="email" placeholder="Email" value={this.state.fields.email} onChange={this.handleChangeFn('email')} />
              {this.state.field_errors.email && <HelpBlock>{this.state.field_errors.email[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup validationState={this.state.field_errors.password && 'error'}>
              <ControlLabel>Password</ControlLabel>
              <FormControl type="password" placeholder="Password" value={this.state.fields.password} onChange={this.handleChangeFn('password')} />
              {this.state.field_errors.password && <HelpBlock>{this.state.field_errors.password[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup validationState={this.state.field_errors.password_verify && 'error'}>
              <ControlLabel>Password Verify</ControlLabel>
              <FormControl type="password" placeholder="Password Verify" value={this.state.fields.password_verify} onChange={this.handleChangeFn('password_verify')} />
              {this.state.field_errors.password_verify && <HelpBlock>{this.state.field_errors.password_verify[0]}</HelpBlock>}
            </FormGroup>

            <FormGroup>
              <Button bsStyle="primary" onClick={this.handleSubmit}>
                Register
                </Button>
            </FormGroup>
          </Form>
        </Panel>
      </Col>
    </Row>);
  }
}

export default Register;
