import React, { Component } from 'react';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Button from 'react-bootstrap/lib/Button';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Panel from 'react-bootstrap/lib/Panel';
import Alert from 'react-bootstrap/lib/Alert';

import validate from 'validate.js';
import auth from '../../apis/auth';
import auth_utils from '../../utils/auth';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        email: '',
        password: '',
      },
      alert: '',
    };

    this.handleChangeFn = this.handleChangeFn.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChangeFn(field) {
    const handleFn = function handleFn(event) {
      const fields = this.state.fields;
      fields[field] = event.target.value;
      this.setState({ fields });
    };
    return handleFn.bind(this);
  }
  validate() {
    this.setState({
      alert: null,
    });
    const errors = validate(this.state.fields, {
      email: {
        presence: true,
        email: true,
      },
      password: {
        presence: true,
      },
    });
    if (errors) {
      setTimeout(() =>
        this.setState({
          alert: 'Error, please check your email and password!',
        })
        , 100);
      return false;
    }
    return true;
  }
  handleSubmit() {
    if (this.validate()) {
      auth.login(this.state.fields)
        .end((err, resp) => {
          if (resp.status !== 200) {
            this.setState({
              alert: 'Error, please check your email and password!',
            });
          } else {
            auth_utils.login(resp.body, 'Login Success!');
          }
        });
    }
  }
  render() {
    return (<Row>
      <Col xs={12} sm={8} smOffset={2} md={6} mdOffset={3}>
        {this.state.alert && <Alert bsStyle="danger">
          {this.state.alert}
        </Alert>}
        <Panel header="Login">
          <Form>
            <FormGroup>
              <ControlLabel>Email</ControlLabel>
              <FormControl type="email" placeholder="Email" value={this.state.fields.email} onChange={this.handleChangeFn('email')} />
            </FormGroup>

            <FormGroup>
              <ControlLabel>Password</ControlLabel>
              <FormControl type="password" placeholder="Password" value={this.state.fields.password} onChange={this.handleChangeFn('password')} />
            </FormGroup>

            <FormGroup>
              <Button bsStyle="primary" onClick={this.handleSubmit}>
                Login
                </Button>
            </FormGroup>
          </Form>
        </Panel>
      </Col>
    </Row>);
  }
}

export default Login;
