import React, { Component } from 'react';
import Grid from 'react-bootstrap/lib/Grid';
import AppBar from './common/AppBar';

import auth_utils from '../utils/auth';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedin: auth_utils.check_login(),
    };
    this.checkStatus = this.checkStatus.bind(this);
  }
  componentWillReceiveProps() {
    this.checkStatus();
  }
  checkStatus() {
    this.setState({
      loggedin: auth_utils.check_login(),
    });
  }
  render() {
    return (
      <div>
        <AppBar loggedin={this.state.loggedin} onRefresh={this.checkStatus} />
        <Grid>
          {React.cloneElement(this.props.children, { loggedin: this.state.loggedin })}
        </Grid>
      </div>
    );
  }
}

export default App;
