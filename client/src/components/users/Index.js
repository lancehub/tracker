import React, { Component } from 'react';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import List from '../common/List';
import Filters from './elements/Filters';
import TableHeader from '../common/TableHeader';

import rest from '../../apis/users';

class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filters: {},
      sort: 'id,desc',
    };
    this.handleFilters = this.handleFilters.bind(this);
    this.handleSort = this.handleSort.bind(this);
    this.searchFn = this.searchFn.bind(this);
  }
  handleFilters(filters) {
    this.setState({ filters });
  }
  handleSort(sort) {
    this.setState({ sort });
  }
  searchFn(page) {
    return rest.index(page, this.state);
  }
  renderItem(user) {
    return (
      <tr key={user.id} onClick={() => (window.location = `#/users/view/${user.id}`)}>
        <td>{user.id}</td>
        <td>{user.name}</td>
        <td>{user.email}</td>
        <td>{user.role}</td>
        <td>{user.created_at}</td>
      </tr>
    );
  }
  render() {
    const tableHeader = (<TableHeader
      onSort={this.handleSort}
      sort={this.state.sort}
      fields={{ id: '#', name: 'Name', email: 'Email', role: 'Role', created_at: 'Register At' }}
    />);
    return (
      <Row>
        <Col xs={12} sm={12} md={12}>
          <Filters filters={this.state.filters} onSearch={this.handleFilters} />
          <List
            tableHeader={tableHeader}
            renderItem={this.renderItem}
            fetchData={this.searchFn}
            sort={this.state.sort}
            filters={this.state.filters}
          />
          <ButtonToolbar>
            <Button onClick={() => (window.location = '#/users/add')}>Add</Button>
          </ButtonToolbar>
        </Col>
      </Row>
    );
  }
}

export default Index;
