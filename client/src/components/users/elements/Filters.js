import React, { Component } from 'react';

import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Button from 'react-bootstrap/lib/Button';
import Glyphicon from 'react-bootstrap/lib/Glyphicon';
import Collapse from 'react-bootstrap/lib/Collapse';
import Well from 'react-bootstrap/lib/Well';

import store from 'store';

class Filters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fields: {
        name: '',
        email: '',
        role: '',
      },
    };
    this.handleChangeFn = this.handleChangeFn.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillMount() {
    this.setState({
      fields: Object.assign({}, this.state.fields, this.props.fields),
    });
  }
  handleChangeFn(field) {
    const handleFn = function handleFn(event) {
      const fields = this.state.fields;
      fields[field] = event.target.value;
      this.setState({ fields });
    };
    return handleFn.bind(this);
  }
  handleSubmit() {
    this.props.onSearch(this.state.fields);
  }
  render() {
    return (
      <div>
        <ButtonToolbar>
          <Button onClick={() => this.setState({ open: !this.state.open })}>
            <Glyphicon glyph="filter" /> Filters
          </Button>
        </ButtonToolbar>
        <br />
        <Collapse in={this.state.open}>
          <div>
            <Well>
              <Form inline>
                <FormGroup controlId="formInlineName">
                  <ControlLabel>Name</ControlLabel>
                  {' '}
                  <FormControl type="text" placeholder="Name" value={this.state.fields.name} onChange={this.handleChangeFn('name')} />
                </FormGroup>
                <span />
                <FormGroup controlId="formInlineEmail">
                  <ControlLabel>Email</ControlLabel>
                  {' '}
                  <FormControl type="email" placeholder="Email" value={this.state.fields.email} onChange={this.handleChangeFn('email')} />
                </FormGroup>
                <span />
                {store.get('role') === 'admin' && <FormGroup>
                  <ControlLabel>Role</ControlLabel>
                  <FormControl componentClass="select" value={this.state.fields.role} onChange={this.handleChangeFn('role')}>
                    <option value="">All</option>
                    <option value="regular">Regular</option>
                    <option value="manager">Manager</option>
                    <option value="admin">Admin</option>
                  </FormControl>
                </FormGroup>}
                <span />
                <Button bsStyle="primary" onClick={this.handleSubmit}>Search</Button>
              </Form>
            </Well>
          </div>
        </Collapse>
      </div>
    );
  }
}

export default Filters;
