import jwt_decode from 'jwt-decode';
import store from 'store';
import flash from './flash';

const auth_utils = {
  login(body, message) {
    const token_decoded = jwt_decode(body.token);
    store.set('token', body.token);
    store.set('token_decoded', token_decoded);
    store.set('id', body.user.id);
    store.set('name', body.user.name);
    store.set('role', body.user.role);
    flash.set('message', message);
    window.location = '#/';
  },
  check_login() {
    const token = store.get('token');
    const token_decoded = store.get('token_decoded');
    return (token && token_decoded && token_decoded.exp >= (+new Date()) / 1000);
  },
  logout() {
    store.clear();
    flash.set('message', 'You are logged out!');
    window.location = '#/';
  },
  require_login() {
    if (!auth_utils.check_login()) {
      window.location = '#/login';
    }
  },
  require_non_regular() {
    auth_utils.require_login();
    if (store.get('role') === 'regular') {
      window.location = '#/';
    }
  },
};

export default auth_utils;
