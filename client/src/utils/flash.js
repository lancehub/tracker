const flash = {
  messages: {},
  set(key, value) {
    flash.messages[key] = value;
  },
  get(key) {
    const value = flash.messages[key];
    delete flash.messages[key];
    return value;
  },
};

export default flash;
