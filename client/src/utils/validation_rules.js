export default {
  user: {
    name: {
      presence: true,
    },
    email: {
      presence: true,
      email: true,
    },
    password: {
      presence: true,
      length: { minimum: 6 },
    },
    role: {
      presence: true,
    },
  },
  expense: {
    description: {
      presence: true,
    },
    amount: {
      presence: true,
      numericality: { lessThanOrEqualTo: 99999999.99 },
      format: {
        pattern: /\d{1,}(\.\d{0,2})?$/,
        message: 'can only be 2 decimal places, like: 2.45',
      },
    },
    pay_at: {
      presence: true,
      format: {
        pattern: /^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/,
        flags: 'i',
        message: 'can only be date time format like: 2016-12-20 20:30:01',
      },
    },
  },
};
