<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expense;
use App\Exceptions\NoPermissionException;

class StatController extends Controller
{
    /**
     * @api {get} /stats stats
     * @apiGroup Stat
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiParam {String} from pay_at >= xx format: 2016-11-21, default is begining of this week
     * @apiParam {String} to pay_at < xx format: 2016-12-01, default is end of this week
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *        "from": "2016-01-01",
     *        "to": "2016-12-12",
     *        "days": "346",
     *        "total": "1000110.00",
     *        "average": "2890.49"
     *      }
     */
    public function index(Request $request)
    {
        if (\Auth::user()->role != 'admin') {
            throw new NoPermissionException;
        }

        $from = $request->input('from');
        $to = $request->input('to');
        $from = !empty($from) ? $from : date('Y-m-d', strtotime('this week') - 24 * 3600);
        $to = !empty($to) ? $to : date('Y-m-d', strtotime('this week') - 24 * 3600 + 7 * 24 * 3600);

        $query = Expense::manageable(\Auth::user());

        $query->where('pay_at', '>=', $from);
        $query->where('pay_at', '<', $to);

        $days = (strtotime($to) - strtotime($from)) / (24 * 3600);
        if ($days < 1) {
            return response()->json(['error' => 'bad_parameters'], 400);
        }

        $total = $query->sum('amount');
        $average = $total / $days;
        $total = number_format($total, 2, '.', '');
        $average = number_format($average, 2, '.', '');

        return response()->json(compact('from', 'to', 'days', 'total', 'average'));
    }

    /**
     * @api {get} /stats/:id Per user stat
     * @apiGroup Stat
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiParam {String} from pay_at >= xx format: 2016-11-21, default is begining of this week
     * @apiParam {String} to pay_at < xx format: 2016-12-01, default is end of this week
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *        "from": "2016-12-05",
     *        "to": "2016-12-12",
     *        "days": "7",
     *        "total": "111.00",
     *        "average": "15.86"
     *      }
     */
    public function show(Request $request, $user_id)
    {
        if (\Auth::user()->role != 'admin' && \Auth::user()->id != $user_id) {
            throw new NoPermissionException;
        }

        $from = $request->input('from');
        $to = $request->input('to');
        $from = !empty($from) ? $from : date('Y-m-d', strtotime('this week') - 24 * 3600);
        $to = !empty($to) ? $to : date('Y-m-d', strtotime('this week') - 24 * 3600 + 7 * 24 * 3600);

        $query = Expense::where('user_id', $user_id);

        $query->where('pay_at', '>=', $from);
        $query->where('pay_at', '<', $to);

        $days = (strtotime($to) - strtotime($from)) / (24 * 3600);
        if ($days < 1) {
            return response()->json(['error' => 'bad_parameters'], 400);
        }

        $total = $query->sum('amount');
        $average = $total / $days;
        $total = number_format($total, 2, '.', '');
        $average = number_format($average, 2, '.', '');

        return response()->json(compact('from', 'to', 'days', 'total', 'average'));
    }
}
