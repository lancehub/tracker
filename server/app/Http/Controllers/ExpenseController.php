<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expense;

class ExpenseController extends Controller
{
    /**
     * @api {get} /expenses Expense list
     * @apiGroup Expense
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiParam {String} mode can be own, or manage, default to be own
     * @apiParam {String} filters[amount_from] amount >= xx
     * @apiParam {String} filters[amount_to] amount <= xx
     * @apiParam {String} filters[pay_at_from] pay_at >= xx
     * @apiParam {String} filters[pay_at_to] pay_at <= xx
     * @apiParam {String} filters[desciption] desciption
     * @apiParam {String} filters[comment] comment
     * @apiParam {String} filters[user_id] user_id
     * @apiParam {String} limit limit per page
     * @apiParam {String} page page
     * @apiParam {String} sort sort like id,desc
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "meta": {
     *              "total": 5,
     *              "per_page": 10,
     *              "current_page": 1,
     *              "last_page": 1,
     *              "next_page_url": null,
     *              "prev_page_url": null,
     *              "from": 1,
     *              "to": 5,
     *          },
     *          "data": [
     *              {
     *                  "id": 1,
     *                  "user_id": 1,
     *                  "pay_at": "2007-12-22 17:57:33",
     *                  "description": "voluptas facilis molestias unde *deserunt.",
     *                  "amount": "8.27",
     *                  "comment": "Temporibus quas non tenetur ab et.",
     *                  "created_at": "2016-11-26 14:13:22",
     *                  "updated_at": "2016-11-26 14:13:22"
     *              }
     *          ]
     *      }
     */
    public function index(Request $request)
    {
        $mode = $request->input('mode', 'own');
        $limit = $request->input('limit', 10);
        $filters = $request->input('filters', []);
        $sort = $request->input('sort', 'id,desc');

        if ($mode == 'manage') {
            $query = Expense::manageable(\Auth::user())->with('user');
        } else {
            $query = Expense::own(\Auth::user());
        }

        if (!empty($filters['amount_from'])) {
            $query->where('amount', '>=', $filters['amount_from']);
        }
        if (!empty($filters['amount_to'])) {
            $query->where('amount', '<=', $filters['amount_to']);
        }
        if (!empty($filters['pay_at_from'])) {
            $query->where('pay_at', '>=', $filters['pay_at_from']);
        }
        if (!empty($filters['pay_at_to'])) {
            $query->where('pay_at', '<=', $filters['pay_at_to']);
        }
        if (!empty($filters['description'])) {
            $query->where('description', 'LIKE', '%'.$filters['description'].'%');
        }
        if (!empty($filters['comment'])) {
            $query->where('comment', 'LIKE', '%'.$filters['comment'].'%');
        }
        if (!empty($filters['user_id'])) {
            $query->where('user_id', $filters['user_id']);
        }

        if (!empty($sort)) {
            list($sort_by, $sort_dir) = explode(',', $sort);
            $query->orderBy($sort_by, $sort_dir);
        }

        $expenses = $query->paginate($limit);

        return response()->json($this->transform($expenses));
    }

    /**
     * @api {get} /expenses/:id Get expense info
     * @apiGroup Expense
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "expense":{
     *               "id": 1,
     *               "user_id": 1,
     *               "pay_at": "2007-12-22 17:57:33",
     *               "description": "voluptas facilis molestias unde *deserunt.",
     *               "amount": "8.27",
     *               "comment": "Temporibus quas non tenetur ab et.",
     *               "created_at": "2016-11-26 14:13:22",
     *               "updated_at": "2016-11-26 14:13:22"
     *          }
     *      }
     */
    public function show($id)
    {
        $expense = Expense::findOrFail($id);
        $expense->checkPermission(\Auth::user());

        return response()->json(compact('expense'));
    }

    /**
     * @api {post} /expenses Create a expense
     * @apiGroup Expense
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiParam {String} user_id non-admin user can only use their own user id
     * @apiParam {String} pay_at pay date time
     * @apiParam {String} amount amount
     * @apiParam {String} [description] description
     * @apiParam {String} [comment] comment
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "expense":{
     *               "id": 1,
     *               "user_id": 1,
     *               "pay_at": "2007-12-22 17:57:33",
     *               "description": "voluptas facilis molestias unde *deserunt.",
     *               "amount": "8.27",
     *               "comment": "Temporibus quas non tenetur ab et.",
     *               "created_at": "2016-11-26 14:13:22",
     *               "updated_at": "2016-11-26 14:13:22"
     *          }
     *      }
     */
    public function store(Request $request)
    {
        $validation_rules = [
                'user_id' => 'required|numeric',
                'description' => 'required|string',
                'pay_at' => 'required|date_format:Y-m-d H:i:s',
                'amount' => 'required|numeric|max:99999999.99|min:0',
            ];
        if (\Auth::user()->role != 'admin') {
            $validation_rules['user_id'] = 'required|numeric|in:'.\Auth::user()->id;
        }
        $this->validate($request, $validation_rules);

        $attributes = $request->only('pay_at', 'description', 'comment');
        $expense = new Expense($attributes);
        $expense->amount = $request->input('amount');
        $expense->user_id = $request->input('user_id');
        $expense->save();

        return response()->json(compact('expense'));
    }

    /**
     * @api {put} /expenses/:id Update expense info
     * @apiGroup Expense
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiParam {String} user_id non-admin user can only use their own user id
     * @apiParam {String} pay_at pay date time
     * @apiParam {String} amount amount
     * @apiParam {String} [description] description
     * @apiParam {String} [comment] comment
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "expense":{
     *               "id": 1,
     *               "user_id": 1,
     *               "pay_at": "2007-12-22 17:57:33",
     *               "description": "voluptas facilis molestias unde *deserunt.",
     *               "amount": "8.27",
     *               "comment": "Temporibus quas non tenetur ab et.",
     *               "created_at": "2016-11-26 14:13:22",
     *               "updated_at": "2016-11-26 14:13:22"
     *          }
     *      }
     */
    public function update(Request $request, $id)
    {
        $validation_rules = [
                'user_id' => 'required|numeric',
                'description' => 'required|string',
                'pay_at' => 'required|date_format:Y-m-d H:i:s',
                'amount' => 'required|numeric|max:99999999.99|min:0',
            ];
        if (\Auth::user()->role != 'admin') {
            $validation_rules['user_id'] = 'required|numeric|in:'.\Auth::user()->id;
        }
        $this->validate($request, $validation_rules);

        $expense = Expense::findOrFail($id);
        $expense->checkPermission(\Auth::user());

        $attributes = $request->only('pay_at', 'description', 'comment');
        $expense->fill($attributes);
        $expense->amount = $request->input('amount');
        $expense->user_id = $request->input('user_id');
        $expense->save();

        return response()->json(compact('expense'));
    }

    /**
     * @api {delete} /expenses/:id Delete expense
     * @apiGroup Expense
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204
     */
    public function destroy($id)
    {
        $expense = Expense::findOrFail($id);
        $expense->checkPermission(\Auth::user());

        $expense->delete();

        return response(null, 204);
    }
}
