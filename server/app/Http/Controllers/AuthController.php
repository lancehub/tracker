<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * @api {post} /auth/login Login
     * @apiGroup Auth
     * @apiPermission none
     * @apiParam {Email} email
     * @apiParam {String} password
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "user":{
     *              "id": 1,
     *              "name": "Cedrick Paucek MD",
     *              "email": "test1@test.com",
     *              "role": "regular",
     *              "created_at": "2016-11-26 01:59:49",
     *              "updated_at": "2016-11-26 01:59:49"
     *          },
     *          "token": "xxxx"
     *      }
     * @apiErrorExample {json} Error-Response:
     *      HTTP/1.1 404 Not Found
     *      {
     *          "error": "user_not_found"
     *      }
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        if (!$token = \Auth::attempt($request->only(['email', 'password']))) {
            return response()->json(['error' => 'user_not_found'], 404);
        }

        $user = \Auth::user();

        return response()->json(compact('user', 'token'));
    }

    /**
     * @api {post} /auth/register Register
     * @apiGroup Auth
     * @apiPermission none
     * @apiVersion 0.1.0
     * @apiParam {String} name name
     * @apiParam {String} email email
     * @apiParam {String} password password
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "user":{
     *              "id": 1,
     *              "name": "Cedrick Paucek MD",
     *              "email": "test1@test.com",
     *              "role": "regular",
     *              "created_at": "2016-11-26 01:59:49",
     *              "updated_at": "2016-11-26 01:59:49"
     *          },
     *          "token": "xxxx"
     *      }
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string',
        ]);

        $attributes = $request->only('name', 'email');
        $user = new User($attributes);
        $user->password = app('hash')->make($request->input('password'));
        $user->role = 'regular';
        $user->save();

        $token = \Auth::fromUser($user);

        return response()->json(compact('user', 'token'));
    }

    /**
     * @api {get} /auth/me Get current user
     * @apiGroup Auth
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "user": {
     *              ....
     *          }
     *      }
     */
    public function me()
    {
        $user = \Auth::user();

        return response()->json(compact('user'));
    }

    /**
     * @api {post} /auth/refresh Refresh token
     * @apiGroup Auth
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiHeader {String} Authorization Bearer the old jwt-token
     * @apiHeaderExample {json} Header-Example:
     *      {
     *          "Authorization": "Bearer xxxx"
     *      }
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "token": "xxxx"
     *      }
     */
    public function refresh()
    {
        $token = \Auth::refresh();

        return response()->json(compact('token'));
    }
}
