<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * re-structure pagination results.
     */
    protected function transform($results)
    {
        $meta = $results->toArray();
        $data = $results->items();
        unset($meta['data']);

        return ['meta' => $meta, 'data' => $data];
    }
}
