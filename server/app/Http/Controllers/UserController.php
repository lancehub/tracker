<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * @api {get} /users User list
     * @apiGroup User
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiParam {String} filters[name] name
     * @apiParam {String} filters[email] email
     * @apiParam {String} filters[role] role
     * @apiParam {String} limit limit per page
     * @apiParam {String} page page
     * @apiParam {String} sort sort like id,desc
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "meta": {
     *              "total": 5,
     *              "per_page": 10,
     *              "current_page": 1,
     *              "last_page": 1,
     *              "next_page_url": null,
     *              "prev_page_url": null,
     *              "from": 1,
     *              "to": 5,
     *          },
     *          "data": [
     *              {
     *                 "id": 1,
     *                 "name": "Cedrick Paucek MD",
     *                 "email": "test1@test.com",
     *                 "role": "regular",
     *                 "created_at": "2016-11-26 01:59:49",
     *                 "updated_at": "2016-11-26 01:59:49"
     *              }
     *          ]
     *      }
     */
    public function index(Request $request)
    {
        $limit = $request->input('limit', 10);
        $filters = $request->input('filters', []);
        $sort = $request->input('sort', 'id,desc');

        $query = User::manageable(\Auth::user());

        if (!empty($filters['name'])) {
            $query->where('name', 'LIKE', '%'.$filters['name'].'%');
        }
        if (!empty($filters['email'])) {
            $query->where('email', 'LIKE', '%'.$filters['email'].'%');
        }
        if (!empty($filters['role'])) {
            $query->where('role', $filters['role']);
        }

        if (!empty($sort)) {
            list($sort_by, $sort_dir) = explode(',', $sort);
            $query->orderBy($sort_by, $sort_dir);
        }

        $users = $query->paginate($limit);

        return response()->json($this->transform($users));
    }

    /**
     * @api {get} /users/:id Get user info
     * @apiGroup User
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "user":{
     *              "id": 1,
     *              "name": "Cedrick Paucek MD",
     *              "email": "test1@test.com",
     *              "role": "regular",
     *              "created_at": "2016-11-26 01:59:49",
     *              "updated_at": "2016-11-26 01:59:49"
     *          }
     *      }
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $user->checkPermission(\Auth::user());

        return response()->json(compact('user'));
    }

    /**
     * @api {post} /users Create a user
     * @apiGroup User
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiParam {String} name name
     * @apiParam {String} email email
     * @apiParam {String} password password
     * @apiParam {String} role can be regular,manager or admin, non-admin user can only create regular user
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "user":{
     *              "id": 1,
     *              "name": "Cedrick Paucek MD",
     *              "email": "test1@test.com",
     *              "role": "regular",
     *              "created_at": "2016-11-26 01:59:49",
     *              "updated_at": "2016-11-26 01:59:49"
     *          }
     *      }
     */
    public function store(Request $request)
    {
        $validation_rules = [
                'name' => 'required|string',
                'email' => 'required|email|unique:users',
                'password' => 'required|string|min:6',
                'role' => 'required|in:regular,manager,admin',
            ];
        if (\Auth::user()->role != 'admin') {
            $validation_rules['role'] = 'required|in:regular';
        }
        $this->validate($request, $validation_rules);

        $attributes = $request->only('name', 'email');
        $user = new User($attributes);
        $user->password = app('hash')->make($request->input('password'));
        $user->role = $request->input('role');
        $user->save();

        return response()->json(compact('user'));
    }

    /**
     * @api {put} /users/:id Update user info
     * @apiGroup User
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiParam {String} name name
     * @apiParam {String} email email
     * @apiParam {String} password password
     * @apiParam {String} role role, can be regular,manager or admin, non-admin usr can only create regular user
     * @apiSuccessExample {json} Success-Response:
     *      HTTP/1.1 200 OK
     *      {
     *          "user":{
     *              "id": 1,
     *              "name": "Cedrick Paucek MD",
     *              "email": "test1@test.com",
     *              "role": "regular",
     *              "created_at": "2016-11-26 01:59:49",
     *              "updated_at": "2016-11-26 01:59:49"
     *          }
     *      }
     */
    public function update(Request $request, $id)
    {
        $validation_rules = [
                'name' => 'required|string',
                'email' => 'required|email|unique:users,email,'.$id,
                'password' => 'required|string|min:6',
                'role' => 'required|in:regular,manager,admin',
            ];
        if (\Auth::user()->role != 'admin') {
            $validation_rules['role'] = 'required|in:regular';
        }
        $this->validate($request, $validation_rules);

        $user = User::findOrFail($id);
        $user->checkPermission(\Auth::user());

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = app('hash')->make($request->input('password'));
        $user->role = $request->input('role');
        $user->save();

        return response()->json(compact('user'));
    }

    /**
     * @api {delete} /users/:id Delete user
     * @apiGroup User
     * @apiPermission JWT
     * @apiVersion 0.1.0
     * @apiSuccessExample {json} Success-Response:
     *   HTTP/1.1 204
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->checkPermission(\Auth::user());

        $user->delete();

        return response(null, 204);
    }
}
