<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Http\Request;

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'api'], function () use ($app) {

    //version
    $app->get('/', function () use ($app) {
        return response()->json(['online' => true]);
    });

    //auth apis
    $app->group([
        'prefix' => 'auth',
    ], function () use ($app) {
        $app->post('login', 'AuthController@login'); //login
        $app->post('register', 'AuthController@register'); //register

        $app->group([
            'middleware' => 'auth',
        ], function () use ($app) {
            $app->get('me', 'AuthController@me'); //get current user
            $app->post('refresh', 'AuthController@refresh'); //refresh token
        });
    });

    //user rest apis
    $app->group([
        'prefix' => 'users',
        'middleware' => 'auth',
    ], function () use ($app) {
        $app->get('/', 'UserController@index');
        $app->get('{id}', 'UserController@show');
        $app->post('/', 'UserController@store');
        $app->put('{id}', 'UserController@update');
        $app->delete('{id}', 'UserController@destroy');
    });

    //expense rest apis
    $app->group([
        'prefix' => 'expenses',
        'middleware' => 'auth',
    ], function () use ($app) {
        $app->get('/', 'ExpenseController@index');
        $app->get('{id}', 'ExpenseController@show');
        $app->post('/', 'ExpenseController@store');
        $app->put('{id}', 'ExpenseController@update');
        $app->delete('{id}', 'ExpenseController@destroy');
    });

    //stats
    $app->group([
        'prefix' => 'stats',
        'middleware' => 'auth',
    ], function () use ($app) {
        $app->get('/', 'StatController@index');
        $app->get('{id}', 'StatController@show');
    });
});
