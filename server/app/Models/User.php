<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Exceptions\NoPermissionException;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function expenses()
    {
        return $this->hasMany('App\Models\Expense');
    }

    /**
     * a quick way to query records that can be managed by current auth user.
     *
     * @param  QueryBuilder
     * @param  User current auth user
     * @return QueryBuilder
     */
    public function scopeManageable($query, $current_user)
    {
        if ($current_user->role == 'admin') {
            return $query;
        }
        if ($current_user->role == 'manager') {
            return $query->where('role', 'regular')->orWhere('id', $current_user->id);
        }

        return $query->where('id', -1);
    }

    /**
     * Check the current auth user can operate this object or not
     *g.
     * @param  User current auth user
     * @throws NoPermissionException
     */
    public function checkPermission($current_user)
    {
        $permission = false;
        if ($current_user->role == 'admin') {
            $permission = true;
        } elseif ($current_user->role == 'manager') {
            $permission = ($this->role == 'regular') || ($this->id == $current_user->id);
        } else {
            $permission = $this->id == $current_user->id;
        }

        if (!$permission) {
            throw new NoPermissionException;
        }
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
