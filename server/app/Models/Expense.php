<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Exceptions\NoPermissionException;

class Expense extends Model
{
    protected $fillable = [
        'pay_at', 'description', 'comment',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * a quick way to query records that can be managed by current auth user.
     *
     * @param  QueryBuilder
     * @param  User current auth user
     * @return QueryBuilder
     */
    public function scopeManageable($query, $current_user)
    {
        if ($current_user->role == 'admin') {
            return $query;
        }

        return $query->where('user_id', '-1');
    }

    /**
     * a quick way to query records that can own by current auth user.
     *
     * @param  QueryBuilder
     * @param  User current auth user
     * @return QueryBuilder
     */
    public function scopeOwn($query, $current_user)
    {
        return $query->where('user_id', $current_user->id);
    }

    /**
     * Check the current auth user can operate this object or not.
     *
     * @param  User current auth user
     * @throws NoPermissionException
     */
    public function checkPermission($current_user)
    {
        $permission = false;
        if ($current_user->role == 'admin') {
            $permission = true;
        } else {
            $permission = $this->user_id == $current_user->id;
        }

        if (!$permission) {
            throw new NoPermissionException;
        }
    }

    /**
     *  Set amount field auto format it.
     * @param $value
     */
    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = number_format(floor($value * 100) / 100, 2, '.', '');
    }
}
