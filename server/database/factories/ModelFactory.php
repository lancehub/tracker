<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => app('hash')->make('123456'),
        'role' => $faker->randomElement(['regular', 'manager', 'admin']),
    ];
});

$factory->define(App\Models\Expense::class, function (Faker\Generator $faker) {
    $user_ids = App\Models\User::all()->pluck('id')->all();

    return [
        'user_id' => $faker->randomElement($user_ids),
        'pay_at' => $faker->dateTime->format('Y-m-d H:i:s'),
        'description' => $faker->sentence,
        'amount' => $faker->randomFloat(2),
        'comment' => $faker->sentence,
    ];
});
