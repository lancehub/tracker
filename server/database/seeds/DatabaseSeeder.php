<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        factory(App\Models\User::class)->create([
            'email' => 'test1@test.com',
            'role' => 'regular',
        ]);
        factory(App\Models\User::class)->create([
            'email' => 'test2@test.com',
            'role' => 'manager',
        ]);
        factory(App\Models\User::class)->create([
            'email' => 'test3@test.com',
            'role' => 'admin',
        ]);

        factory(App\Models\Expense::class, 10)->create();
    }
}
