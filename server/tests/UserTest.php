<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\User;
use Faker\Factory;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex()
    {
        $user = User::where('email', 'test1@test.com')->first();
        $this->be($user);

        $this->json('GET', 'api/users');

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => ['total', 'per_page', 'current_page', 'last_page'],
        ]);
        $this->seeJson(['data' => []]);

        $user2 = User::where('email', 'test2@test.com')->first();
        $this->be($user2);

        $this->json('GET', 'api/users');

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => ['total', 'per_page', 'current_page', 'last_page'],
            'data' => ['*' => ['id', 'name', 'email', 'role', 'created_at', 'updated_at']],
        ]);
    }

    public function testShow()
    {
        $user = User::where('email', 'test2@test.com')->first(); //manager user
        $this->be($user);

        $user2 = User::where('email', 'test1@test.com')->first();

        $this->json('GET', 'api/users/'.$user2->id);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'user' => ['id', 'name', 'email', 'role', 'created_at', 'updated_at'],
        ]);
    }

    public function testCreate()
    {
        $user = User::where('email', 'test2@test.com')->first();
        $this->be($user);

        $faker = Faker\Factory::create();
        $email = $faker->email;

        //test bad data
        $data = [];
        $this->json('POST', 'api/users', $data);
        $this->seeStatusCode(400);
        $this->seeJson(['error' => 'validation_error']);

        //test create user
        $data = [
            'name' => $faker->name,
            'email' => $email,
            'password' => '123456',
            'role' => 'manager',
        ];

        //test with wrong role
        $this->json('POST', 'api/users', $data);
        $this->seeStatusCode(400);
        $this->seeJson(['error' => 'validation_error']);

        //test success create
        $data['role'] = 'regular';
        $this->json('POST', 'api/users', $data);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'user' => ['id', 'name', 'email', 'role', 'created_at', 'updated_at'],
        ]);
        $this->seeInDatabase('users', ['email' => $email]);

        //test duplicate submission
        $this->json('POST', 'api/users', $data);
        $this->seeStatusCode(400);
        $this->seeJson(['error' => 'validation_error']);
    }

    public function testUpdate()
    {
        $user = User::where('email', 'test2@test.com')->first();
        $this->be($user);

        $faker = Faker\Factory::create();
        $name = $faker->name;
        $email = $faker->email;

        $data = [
            'name' => $name,
            'email' => $email,
            'password' => '123456',
            'role' => 'regular',
        ];

        $user2 = User::where('email', 'test1@test.com')->first();
        $this->json('PUT', 'api/users/'.$user2->id, $data);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'user' => ['id', 'name', 'email', 'role', 'created_at', 'updated_at'],
        ]);
        $this->seeInDatabase('users', ['email' => $email, 'name' => $name]);
    }

    public function testDelete()
    {
        $user = User::where('email', 'test2@test.com')->first();
        $this->be($user);

        $user2 = User::where('email', 'test1@test.com')->first();
        $this->json('DELETE', 'api/users/'.$user2->id);

        $this->seeStatusCode(204);
        $this->notSeeInDatabase('users', ['email' => 'test1@test.com']);
    }
}
