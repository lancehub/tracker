<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\User;

class AuthTest extends TestCase
{
    use DatabaseTransactions;

    protected $email;

    public function setUp()
    {
        parent::setUp();

        $this->email = 'test'.uniqid().'@test.com';
    }

    public function testRegisterError()
    {
        $this->json('POST', 'api/auth/register');
        $this->seeStatusCode(400);
        $this->seeJson(['error' => 'validation_error']);
    }

    public function testRegisterSuccess()
    {
        $this->json('POST', 'api/auth/register', [
                'name' => 'Test',
                'email' => $this->email,
                'password' => '123456',
            ]);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'user' => ['id', 'name', 'email', 'role'],
            'token',
        ]);
        $this->seeInDatabase('users', ['email' => $this->email]);
    }

    public function testLoginError()
    {
        $this->json('POST', 'api/auth/login', [
                'email' => 'test1@test.com',
                'password' => '123456888',
            ]);
        $this->seeStatusCode(404);
        $this->seeJson(['error' => 'user_not_found']);
    }

    public function testLoginSuccess()
    {
        $this->json('POST', 'api/auth/login', [
                'email' => 'test1@test.com',
                'password' => '123456',
            ]);

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'token',
        ]);
    }

    public function testMeError()
    {
        $this->json('GET', 'api/auth/me');
        $this->seeStatusCode(401);
        $this->seeJson(['error' => 'unauthorized']);
    }

    public function testMeSuccess()
    {
        $user = User::where('email', 'test1@test.com')->first();
        $this->be($user);

        $this->json('GET', 'api/auth/me');

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'user' => ['id', 'name', 'email', 'role'],
        ]);
        $this->seeJson([
            'email' => 'test1@test.com',
        ]);
    }
}
