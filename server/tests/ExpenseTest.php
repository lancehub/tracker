<?php

use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\User;
use App\Models\Expense;
use Faker\Factory;

class ExpenseTest extends TestCase
{
    use DatabaseTransactions;

    public function testIndex()
    {
        $user = User::where('email', 'test1@test.com')->first();
        $this->be($user);

        factory(App\Models\Expense::class, 10)->create([
            'user_id' => $user->id,
        ]);

        $this->json('GET', 'api/expenses');

        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'meta' => ['total', 'per_page', 'current_page', 'last_page'],
            'data' => ['*' => ['id', 'user_id', 'pay_at', 'amount', 'description', 'comment', 'created_at', 'updated_at']],
        ]);
    }

    public function testShow()
    {
        $user = User::where('email', 'test2@test.com')->first();
        $this->be($user);

        $id = factory(App\Models\Expense::class)->create([
            'user_id' => $user->id,
        ])->id;

        $this->json('GET', 'api/expenses/'.$id);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'expense' => ['id', 'user_id', 'pay_at', 'amount', 'description', 'comment', 'created_at', 'updated_at'],
        ]);
    }

    public function testCreate()
    {
        $user = User::where('email', 'test2@test.com')->first();
        $this->be($user);

        $faker = Faker\Factory::create();
        $email = $faker->email;

        //test bad data
        $data = [];
        $this->json('POST', 'api/expenses', $data);
        $this->seeStatusCode(400);
        $this->seeJson(['error' => 'validation_error']);

        //test create expense
        $data = [
            'pay_at' => $faker->dateTime->format('Y-m-d H:i:s'),
            //'amount' => $faker->randomFloat(2, 0, 99999999.99),
            'description' => $faker->sentence,
            'user_id' => $user->id,
        ];

        //test with bad data
        $this->json('POST', 'api/expenses', $data);
        $this->seeStatusCode(400);
        $this->seeJson(['error' => 'validation_error']);

        //test success create
        $data['amount'] = $faker->randomFloat(2, 0, 99999999.99);
        $this->json('POST', 'api/expenses', $data);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'expense' => ['id', 'user_id', 'pay_at', 'amount', 'description', 'comment', 'created_at', 'updated_at'],
        ]);
    }

    public function testUpdate()
    {
        $user = User::where('email', 'test2@test.com')->first();
        $this->be($user);

        $id = factory(App\Models\Expense::class)->create([
            'user_id' => $user->id,
        ])->id;

        $faker = Faker\Factory::create();

        //test update expense
        $data = [
            'pay_at' => $faker->dateTime->format('Y-m-d H:i:s'),
            'amount' => $faker->randomFloat(2, 0, 99999999.99),
            'description' => $faker->sentence,
            'user_id' => $user->id,
        ];

        $this->json('PUT', 'api/expenses/'.$id, $data);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'expense' => ['id', 'user_id', 'pay_at', 'amount', 'description', 'comment', 'created_at', 'updated_at'],
        ]);
        $this->seeInDatabase('expenses', ['id' => $id] + $data);
    }

    public function testDelete()
    {
        $user = User::where('email', 'test2@test.com')->first();
        $this->be($user);

        $id = factory(App\Models\Expense::class)->create([
            'user_id' => $user->id,
        ])->id;

        $this->json('DELETE', 'api/expenses/'.$id);

        $this->seeStatusCode(204);
        $this->notSeeInDatabase('expenses', ['id' => $id]);
    }
}
